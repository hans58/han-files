CREATE USER 'leo'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'ramon'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'adriann'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'edsel'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'jacob'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'rocel'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'johnRev'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'rodney'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'jerry'@'localhost' IDENTIFIED BY 'password'; 
CREATE USER 'bbJane'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON *.* TO leo@'localhost';
GRANT SELECT, UPDATE ON navy.* TO ramon@'localhost';
GRANT INSERT, DELETE ON army.* TO adriann@'localhost';
GRANT UPDATE, DELETE ON police.* TO edsel@'localhost';
GRANT SELECT, DELETE ON airForce.* TO jacob@'localhost';

GRANT SELECT, INSERT ON spaceForce.* TO rocel@'localhost';

GRANT INSERT, DELETE ON spaceForce.* TO 'johnRev'@'localhost';
GRANT UPDATE, DELETE ON airForce.* TO 'rodney'@'localhost';
GRANT SELECT, DELETE ON police.* TO 'jerry'@'localhost';
GRANT INSERT, UPDATE ON army.* TO 'bbJane'@'localhost';



GRANT permission1, permission2 ON database_name.table_name TO 'database_user'@'localhost';
GRANT ALL PRIVILEGES ON database_name.* TO 'database_user'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'database_user'@'localhost';
GRANT ALL PRIVILEGES ON database_name.table_name TO 'database_user'@'localhost';
GRANT SELECT, INSERT, DELETE ON database_name.* TO database_user@'localhost';


https://www.digitalocean.com/community/tutorials/how-to-set-up-a-remote-database-to-optimize-site-performance-with-mysql

======OMSIM======
SELECT user.id, user.username, user_type.type FROM user
LEFT JOIN user_type ON user.user_type_id = user_type.id 
INTO OUTFILE '/var/lib/mysql-files/1.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';


SELECT user.id, user.username, user_type.type, IF (status = 1, "active", "inactive") AS status FROM user LEFT JOIN user_type ON user.user_type_id = user_type.id WHERE user_type.level > 0
INTO OUTFILE '/var/lib/mysql-files/2.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';