<!-- Installing Laravel-->
1. **Install LAMP
- sudo apt update
- sudo apt install apache2
- sudo ufw allow "Apache Full"
- sudo apt install curl
- curl http://icanhazip.com
- sudo apt install mysql-server
2. sudo apt install phpmyadmin php-mbstring
3. sudo nano /etc/apache2/apache2.conf | input this in the last part: Include /etc/phpmyadmin/apache.conf
4. sudo apt update
5. sudo apt install curl php-cli php-mbstring php-curl git unzip
6. cd ~
   curl -sS https://getcomposer.org/installer -o composer-setup.php
7. HASH=55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae (https://composer.github.io/pubkeys.html)
8. php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
*** The output should be "Installer verified" ***
9. sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
10. composer -- to check if the composer is installed
11. composer create-project laravel/laravel 'name'
12. *ls *cd to your created laravel project
12. php artisan serve --host 0.0.0.0

<!-- To View -->
1. Go to your EC2
2. Edit your inbound rule
- Add custom TCP
- Port range: 8000
- Anywhere IPv4
3. open Public IP Address -- [to view apache]
4. open Public IP Address -- [to view apache]
3. open Public IP Address -- [to view apache]



<!-- LINK -->
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04#step-2-downloading-and-installing-composer