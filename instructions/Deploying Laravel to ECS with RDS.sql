<!-- Deploying Laravel to ECS with RDS --!>


<!-- Laravel to docker with RDS--!>
1. Create laravel
- composer create-project laravel/laravel "name"
2. Create docker-compose.yml, Dockerfile, Dockerfile_Nginx
- change php version to 8.1.7
- delete redis in docker-compose
3. Create folder inside config
- nginx -> conf.d -> app.conf
- php - local.ini
- app.conf (file)
4. Setup and connect Laravel to RDS
- Setup RDS in AWS
- Get the endpoint to connect to MySQL workbench
- Go to .env (laravel)
- DB_HOST="endpoint"
- DB_DATABASE="name that you will create inside workbench"
- DB_USERNAME && DB_PASSWORD ="created on RDS"
5. Install Laravel bootstrap
- composer require laravel/ui
- php artisan ui bootstrap --auth
- npm install && npm run dev
- php artisan migrate

<!-- Laravel--!>
- access IAM
- use aws configure


<!-- Laravel to docker with RDS--!>
6. Push laravel inside docker

7. docker-compose build
8. docker-compose up -d
9. Check your docker if it is running and correct UI
10. After verfiying, push to ECR
11. Go to ECR commands using GitBash
12. Push app first
13. Push nginx using code below:
- docker build . -f Dockerfile_Nginx -t laravel-nginx-ejt

<!-- ECR to ECS--!>
14. Create ECS
15. Create task definitions
- EC2
- Task definition name: ecs-rds-ejt
- Task role: ecsTaskExecutionRole
- Network mode: Default
- Task role: ecsTaskExecutionRole
- Add Container
	*app
	~ Container name: app = *mandatory*
	~ Image: [check ECR]
	~ Memory Limits: 128
	~ Cloudwatch logs: tick

	Skip Step 2 in push commands and replace it with this
	"Push first using 
		docker build . -f Dockerfile_Nginx -t <repo name>"
	*nginx
	~ Container name: nginx
	~ Image: [check ECR]
	~ Memory limits: 128
	~ Port Mappings: 0 , 80
	~ Environment: Key = APP_KEY | Values = [check .env]
	~ Container name: app | START
	~ Links: app
	~ Cloudwatch logs: tick
16. Create cluster
- EC2 Linux + Networking
- EC2 Instance Type: t2.micro
- VPC: Choose existing
	* Select all subnets
- Auto assign public IP: Enabled
- Security group: Choose existing
17. Create Application Load Balancer
- Load balancer name: ecs-rds-ejt
- VPC: [choose existing] | *select all mappings*
- Security group: Choose existing
18. Create Target Group
- Target type: Instances
- Target group name: ecs-rds-ejt
19. Create Services
- Launch type: EC2
- Task Definition: [select the one that you created]
- Load balancer: Application Load balancer
- Load balancer name: *select that you created
- Container to load balance: *select existing
