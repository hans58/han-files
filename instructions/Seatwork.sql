1. all user with fullname, phone, facebook, current points above 100 and below 1000

	SELECT concat(last_name, " ",first_name) as fullname, phone, facebook, current_points FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE current_points >= 100 AND current_points <= 1000;
2. count of distinct user with current points 1000 and above

	SELECT COUNT(users.id)FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE current_points >= 100;
3. count of "meron" bet with win status for date of 2022-01-02, count of "wala" bet with lost status for date of 2022-01-02, count of "draw" for date of 2022-01-02

	SELECT COUNT(side), side FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE side = "meron" AND status = "win"
    OR side = "wala" AND status ="lost"
    OR side = "draw" OR date_processed = 2022-01-02
    GROUP BY side;
4. all user with draw bets for 2022-01-02

	SELECT CONCAT_WS(' ',last_name,first_name) as fullname, side as bets, date_processed FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE side = "draw" OR date_processed = 2022-01-02;
5. all bets with above 1000 and less than 50000

	SELECT CONCAT_WS(' ',last_name,first_name) as fullname, amount as bets FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE amount >= 1000 AND amount <=50000;
6. all verified user with current points above 1000
	
	SELECT username, mobile_verified FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE current_points >= 1000 AND mobile_verified = 0;
7. all username starting from d to e
	
	SELECT username FROM users
	WHERE username REGEXP '^[d-e]' 
8. all bets of user id of 3158401, 2846581, 3304219

	SELECT user_id, username, amount FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE user_id IN ('3158401', '2846581', '3304219');
9.count of user with picture_in_verified
	1 = verified
	2 = need to update
	3 = submitted for approval
	4 = disapproved
	0 = unprocessed

	SELECT COUNT(picture_id_verified) AS USER,
	(CASE picture_id_verified
	WHEN '1' THEN 'VERIFIED' 
	WHEN '2' THEN 'NEED TO UPDATE'
	WHEN '3' THEN 'SUBMITTED FOR APPROVAL'
	WHEN '4' THEN 'DISAPPROVED'
	ELSE 'UNPROCESSED' END) as STATUS 
	FROM users_details GROUP BY picture_id_verified;
10. user with user addresses with picture in verified = 1

	SELECT users.id, username, CONCAT_WS(' ',last_name,first_name) as fullname, 
	CONCAT_WS(' ', permanent_zip_code, permanent_address, permanent_barangay, permanent_city) as address FROM users
	JOIN users_addresses ON users.id = users_addresses.user_id;