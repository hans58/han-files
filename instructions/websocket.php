<!-- Install LAMP  -->

	-sudo apt update
	-sudo apt upgrade
	-sudo apt install apache2
	-sudo apt install software-properties-common
	-sudo add-apt-repository ppa:ondrej/php
	-sudo apt install composer

<!--  -->

	-sudo apt install phpmyadmin php-mbstring
	-sudo nano /etc/apache2/apache2.conf | input this in the last part: Include /etc/phpmyadmin/apache.conf
	
	-sudo apt update
	-sudo apt install curl php-cli php-mbstring php-curl git unzip
	-cd ~
	-curl -sS https://getcomposer.org/installer -o composer-setup.php
	-HASH=55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae
	
	-php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	
	********The output should be "Installer verified"
	-sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
	-composer
	-composer create-project laravel/laravel [project name]
	-php artisan serve --host 0.0.0.0	

<!-- Edit -->

cd /etc/apache2/sites-available
	
	Input this:

		<VirtualHost *:80>
		        ServerName 54.249.98.178
		        ServerAdmin admin@localhost
		        DocumentRoot /var/www/html/websocket-demo/public/
		        <Directory /var/www/html/websocket-demo/public/>
		                AllowOverride All
		                Options Indexes FollowSymLinks
		                Require all granted
		        </Directory>

		        ErrorLog ${APACHE_LOG_DIR}/error.log
		        CustomLog ${APACHE_LOG_DIR}/access.log combined
		</VirtualHost>

<!-- Create DB -->

sudo mysql

edit .env

<!-- Install Websocket -->

-composer require beyondcode/laravel-websockets
-php artisan vendor:publish --provider="BeyondCode\LaravelWebSockets\WebSocketsServiceProvider" --tag="migrations"

-php artisan vendor:publish --provider="BeyondCode\LaravelWebSockets\WebSocketsServiceProvider" --tag="config"


<!-- To View -->
open 2 putty

php artisan serve --host 0.0.0.0
php artisan websockets:serve --host 0.0.0.0
