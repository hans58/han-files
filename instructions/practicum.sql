-- 1 Give me the average of all current points of the users
	SELECT AVG(current_points) From users;

-- 2 Give me user_id, fullname,payout, amount, location and sort the payout descending between 100 and 1000
	SELECT user_id, concat(first_name, " ", last_name) as fullname, payout, amount, (amount - payout) as profit, timezone as location from users
	LEFT JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE payout between 100 and 1000  
	ORDER BY `bets_logs`.`payout`  DESC

-- 3 Give me the username, last log-in ip, id (amount and payout) equals to profit from the users that starts with letter a to f
	SELECT users.id, username, last_login_ip, amount, payout, (amount - payout) as profit from users
	LEFT JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE username LIKE "a%" OR  
		username LIKE "b%"  OR
    	username LIKE "c%"  OR
    	username LIKE "d%"  OR
    	username LIKE "e%"  OR
    	username LIKE "f%"  
	ORDER BY `users`.`username` ASC

-- 4 Return the number of players who placed their bet on wala from fight #1044
	SELECT fight_id, Count(side) from users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE fight_id = 1044 AND side = "wala";

	SELECT fight_id, Count(side) from bets_logs
	WHERE fight_id = 1044 AND side = "wala";

-- 5 Display the id, username and fullname of users who have bets amounting to 1000 and above from asia
	SELECT users.id, username, concat(first_name, " ", last_name) as 	fullname, amount, timezone from users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE amount >= 1000 AND timezone LIKE "%asia%" 
	

-- 6 Display the users whose profit is above 500
	SELECT concat(first_name, " ", last_name) as fullname, income from users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE income >= 500

