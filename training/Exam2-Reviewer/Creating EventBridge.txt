
1. CREATING EVENTBRIDGE
Step 1: Go to EventBridge in search bar
Step 2: Click "Create event bus"  
Step 3: Input name (apf-event-bus-demo)
Step 4: Then CREATE

-----------------------------------------------------

2. CREATING RULES
Step 1: Go to "Rules"
Step 2: Select your event bus (as per sir Ruel's demo, he selected the default event)
Step 3: Input Rule name (apf-eventbridge-rule)
Step 4: Input Description (apf-eventbridge-rule)
Step 5: Select your event bus (default)
Step 6: Select "Rule with an event pattern"
Step 7: CLick Next
Step 8: Select "AWS events or ......" (Event source)
Step 9: Select "AWS events" (Sample Event)
Step 10: Select "EC2 Instance State-change Notification", Sample event 4 (Stopped)
Step 11: Select "AWS services" (Event Source)
Step 12: Select "EC2" (AWS service)
Step 13: Select "EC2 Instance State-change Notification" (Event type)
Step 14: Select "Specific state(s)", "Stopped" ,"terminated"
Step 15: Click Next
Step 16: Select "AWS service" (Target types)
Step 17: Select "SNS topic"
Step 18: Select your created SNS topic
Step 19: Click Next
Step 20: Default tags 
Step 21: Click Next
Step 22: Click CREATE RULE

*You need to create SNS to connect to Step 18. 
--------------------------------------------------------
 
3. TO TEST
Step 1: Create a EC2 Instance on the same region
Step 2: Stop the instance
Step 3: Termnate the instance

-------------------------------------------------------




 